

public class HASE extends SPIELAUTOMAT
{
    
    // x:50, y:480, r:10; f:grau
    
    public HASE(int x, int y, int r, String f)
    {
        xPos=x;
        yPos=y;
        radius=r;
        farbe=f;

        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+1,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+2,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+3,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+4,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+5,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+6,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+18,yPos-8,radius-2,farbe);//Kopf//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+18,yPos-13,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+14,yPos-14,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+13,yPos-15,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+12,yPos-16,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+11,yPos-17,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+10,yPos-18,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos-5,yPos+11,radius-6,farbe);//Bein//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+10,yPos+11,radius-6,farbe);//Bein//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos-10,yPos-1,radius-7,farbe);//Puschel//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+19,yPos-10,radius-8,"schwarz");//Auge//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+25,yPos-6,radius-8,"weiss");//Schnauze//
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+20,yPos-7,xPos+23,yPos-6);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+20,yPos-5,xPos+23,yPos-6);//Schnurrhaare// 
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+27,yPos-6,xPos+29,yPos-7);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+27,yPos-6,xPos+29,yPos-5);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos,yPos+5,radius-8,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos-2,yPos-3,radius-8,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+5,yPos,radius-9,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+8,yPos-4,radius-9,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+11,yPos+3,radius-8,"weiss");//Flecken//
    }
    
    public void loesche()
    {
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+1,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+2,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+3,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+4,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+5,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+6,yPos,radius);//K�rper//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+18,yPos-8,radius-2);//Kopf//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+18,yPos-13,radius-7);//Ohr//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+14,yPos-14,radius-7);//Ohr//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+13,yPos-15,radius-7);//Ohr//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+12,yPos-16,radius-7);//Ohr//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+11,yPos-17,radius-7);//Ohr//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+10,yPos-18,radius-7);//Ohr//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos-5,yPos+11,radius-6);//Bein//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+10,yPos+11,radius-6);//Bein//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos-10,yPos-1,radius-7);//Puschel//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+19,yPos-10,radius-8);//Auge//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+25,yPos-6,radius-8);//Schnauze//
        ZEICHENFENSTER.gibFenster().loescheStrecke(xPos+20,yPos-7,xPos+23,yPos-6);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().loescheStrecke(xPos+20,yPos-5,xPos+23,yPos-6);//Schnurrhaare// 
        ZEICHENFENSTER.gibFenster().loescheStrecke(xPos+27,yPos-6,xPos+29,yPos-7);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().loescheStrecke(xPos+27,yPos-6,xPos+29,yPos-5);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos,yPos+5,radius-8);//Flecken//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos-2,yPos-3,radius-8);//Flecken//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+5,yPos,radius-9);//Flecken//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+8,yPos-4,radius-9);//Flecken//
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos+11,yPos+3,radius-8);//Flecken//
    }
    public void zeichne()
    {
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+1,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+2,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+3,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+4,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+5,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+6,yPos,radius,farbe);//K�rper//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+18,yPos-8,radius-2,farbe);//Kopf//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+18,yPos-13,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+14,yPos-14,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+13,yPos-15,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+12,yPos-16,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+11,yPos-17,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+10,yPos-18,radius-7,farbe);//Ohr//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos-5,yPos+11,radius-6,farbe);//Bein//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+10,yPos+11,radius-6,farbe);//Bein//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos-10,yPos-1,radius-7,farbe);//Puschel//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+19,yPos-10,radius-8,"schwarz");//Auge//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+25,yPos-6,radius-8,"weiss");//Schnauze//
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+20,yPos-7,xPos+23,yPos-6);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+20,yPos-5,xPos+23,yPos-6);//Schnurrhaare// 
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+27,yPos-6,xPos+29,yPos-7);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().zeichneStrecke(xPos+27,yPos-6,xPos+29,yPos-5);//Schnurrhaare//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos,yPos+5,radius-8,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos-2,yPos-3,radius-8,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+5,yPos,radius-9,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+8,yPos-4,radius-9,"weiss");//Flecken//
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos+11,yPos+3,radius-8,"weiss");//Flecken//
    }
    
}
