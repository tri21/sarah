
/**
 * Write a description of class SPUCKE here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SPUCKE extends SPIELAUTOMAT
{
    public SPUCKE(int x, int y, int r, String f)
    {
        xPos=x;
        yPos=y;
        radius=r;
        farbe=f;
     
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos, yPos, radius, farbe);
    }

    public void loesche()
    {
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos,yPos,radius);
    }
    public void zeichne()
    {
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos,yPos,radius,farbe);
    }
}
