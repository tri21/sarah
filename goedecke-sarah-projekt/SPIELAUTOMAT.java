public abstract class SPIELAUTOMAT
{
    protected int xPos, yPos, radius;
    protected String farbe;
    
    public SPIELAUTOMAT()
    {
        
    }

    

    public int gibX(){return xPos;}
   
    public int gibY(){return yPos;}
    
    public int gibR(){return radius;}
    
    public String gibFarbe() {return farbe;}
    
   
    public void bewegeX(int x)
    {
        xPos=xPos+x;
        zeichne();
    }
    
       
    public void bewegeY(int y)
    {
        yPos=yPos+y;
        zeichne();
    }
    
    public void setzeFarbe(String f){
        farbe = f;
    }
   
    public abstract void loesche();
   
    public abstract void zeichne();
    
    
    public void setAusgangspunktKugel(int x, int y)
    {
        xPos=x;
        yPos=y;
    }
    
    public void setAusgangspunktGegner(int x, int y)
    {
        xPos=x;
        yPos=y;
    }
    
    public void setAusgangspunktKasten(int x, int y)
    {
        xPos=x;
        yPos=y;
    }
    
    public void setAusgangspunktKugel2(int x, int y)
    {
        xPos=x;
        yPos=y;
    }
    
    public void setAusgangspunktSturz(int x, int y)
    {
        xPos=x;
        yPos=y;
    }
    
}
