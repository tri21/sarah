
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class SPIEL implements KeyListener
{

    private SPIELAUTOMAT hindernis1,hindernis2, hase, spucke;
    private HINTERGRUND hintergrund;
    private Timer uhr;
    private int vy, vHindernis, vSpucke, leben, level, hindernis;
    private String farbe;
    private JLabel labelLevel, labelLeben, labelHindernisse;
    private String fensterStatus;
    private boolean endlosEin=true;
    
    public SPIEL(){
        ZEICHENFENSTER.gibFenster().setzeTitel("Spucki");
        ZEICHENFENSTER.gibFenster().gibFrame().addKeyListener(this);
        fensterStatus = "Einleitung";
        zeigeEinleitung();

        
    }
    
    public void zeigeEinleitung() {

        //ZEICHENFENSTER.gibFenster().gibFrame().addKeyListener(this);
        
        uhr=new Timer(50, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                ZEICHENFENSTER.gibFenster().fuelleRechteck(0,0,600,600,"weiss");
                hase = new HASE(80, 200, 10, "grau");
                ZEICHENFENSTER.gibFenster().zeichneText("Spucki, das Karnickel", 160, 160);
                ZEICHENFENSTER.gibFenster().zeichneText("#################", 160, 180);
                ZEICHENFENSTER.gibFenster().zeichneText("Herzlich Willkommen bei Spucki, dem Karnickel.", 160, 220);
                ZEICHENFENSTER.gibFenster().zeichneText("Helfen Sie Spucki, die Hindernisse zu �berwinden. ", 160, 240);
                ZEICHENFENSTER.gibFenster().zeichneText("Kleine Hindernisse k�nnen Sie �berspringen.", 160, 260);
                ZEICHENFENSTER.gibFenster().zeichneText("Gro�e rote Hindernisse m�ssen Sie wegspucken.", 160, 280);
                ZEICHENFENSTER.gibFenster().zeichneText("Viel Spa� beim Spielen!", 160, 300);
                ZEICHENFENSTER.gibFenster().zeichneText("", 160, 320);
                ZEICHENFENSTER.gibFenster().zeichneText("Dr�cken Sie jetzt Enter, um die Anleitung zu sehen.", 160, 340);
            }
        });
        uhr.start();        

    }
    
    public void zeigeAnleitung(){      
        //ZEICHENFENSTER.gibFenster().gibFrame().addKeyListener(this);
        
        uhr=new Timer(50, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                ZEICHENFENSTER.gibFenster().fuelleRechteck(0,0,600,600,"weiss");
                hase = new HASE(80, 200, 10, "grau");
                ZEICHENFENSTER.gibFenster().zeichneText("So spielen Sie:", 160, 160);
                ZEICHENFENSTER.gibFenster().zeichneText("###########", 160, 180);
                ZEICHENFENSTER.gibFenster().zeichneText("Pfeil oben: Springen", 160, 220);
                ZEICHENFENSTER.gibFenster().zeichneText("Pfeil rechts: Spiel starten", 160, 240);
                ZEICHENFENSTER.gibFenster().zeichneText("Pfeil links: Spiel unterbrechen", 160, 260);
                ZEICHENFENSTER.gibFenster().zeichneText("S-Taste: Spucken", 160, 280);
                ZEICHENFENSTER.gibFenster().zeichneText("ESC-Taste: Spiel beenden", 160, 300);
                ZEICHENFENSTER.gibFenster().zeichneText("", 160, 320);
                ZEICHENFENSTER.gibFenster().zeichneText("Dr�cken Sie jetzt Enter, um die Anleitung zu beenden.", 160, 340);
            }
        });
        uhr.start();        
    }
    
    public void zeigeNochmalSpielen(){
        uhr.stop();
        
        uhr=new Timer(50, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                ZEICHENFENSTER.gibFenster().fuelleRechteck(0,0,600,600,"weiss");
                ZEICHENFENSTER.gibFenster().zeichneText("Sie haben verloren!", 160, 160);
                ZEICHENFENSTER.gibFenster().zeichneText("###############", 160, 180);
                ZEICHENFENSTER.gibFenster().zeichneText("Wenn Sie nochmal von vorne beginnen wollen,", 160, 220);
                ZEICHENFENSTER.gibFenster().zeichneText("dr�cken Sie bitte die Enter-Taste.", 160, 240);
                ZEICHENFENSTER.gibFenster().zeichneText(" ", 160, 260);
                ZEICHENFENSTER.gibFenster().zeichneText("Ansonsten dr�cken Sie bitte die ESC-Taste.", 160, 280);
                ZEICHENFENSTER.gibFenster().zeichneText(" ", 160, 300);
                ZEICHENFENSTER.gibFenster().zeichneText("Vielen Dank f�rs Spielen!", 160, 320);
            }
        });
        uhr.start();               
    }
    
    public void erstelleLabel(){
        leben = 5;
        level = 1;
        hindernis = 10;
                
        labelLevel = new JLabel("  Level "+level+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(labelLevel, "unten");
        labelLeben = new JLabel("Leben: "+leben+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(labelLeben, "unten");
        labelHindernisse = new JLabel("Hindernisse: Noch "+hindernis+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(labelHindernisse, "unten");        
    }
    
    public void resetLabel() {
        leben = 5;
        level = 1;
        hindernis = 10;
        
        labelLevel.setText("Level: " + level + " | ");
        labelLeben.setText("Leben: "+leben+"  |  ");
        labelHindernisse.setText("Hindernisse: Noch "+hindernis+"  |  ");
        
    }
    
    public void starteSpiel(){       
        
        hintergrund = new HINTERGRUND();
        
        spucke = new SPUCKE(65, 470, 2, "blau");
        
        hindernis1 = new HINDERNIS(500,460,40,"grau");
        hindernis2 = new HINDERNIS(1000,460,40,"schwarz");
        
        hase = new HASE(50, 480, 10, "grau");

        vSpucke=0;
        vHindernis=0;
        
        vy=0;
        
        
        //ZEICHENFENSTER.gibFenster().gibFrame().addKeyListener(this);
        
        uhr=new Timer(50, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                if(hase.gibFarbe() == "rot") {stehen();}                
                
                if (leben == 0) sterben();
                else if (level < 999 && leben > 0)  bewegung();
                
                if(spucke.gibX() > 600){
                    vSpucke = 0;
                    spucke = new SPUCKE(65, 470, 2, "blau");
                }

                if(hindernis1.gibR() == 100){
                    if(hindernis1.gibX() <= spucke.gibX() && vSpucke > 0){
                        loescheHindernis1();
                        vSpucke = 0;
                        spucke = new SPUCKE(65, 470, 2, "blau");
                        setzeHindernisAnzahl();
                    }
                }
                
                if(hindernis2.gibR() == 100){
                    if(hindernis2.gibX() <= spucke.gibX() && vSpucke > 0){
                        loescheHindernis2();
                        vSpucke = 0;
                        spucke = new SPUCKE(65, 470, 2, "blau");
                        setzeHindernisAnzahl();
                    }
                }
                
                if(hase.gibY() < 420) {
                    vy=4;
                }
                
                if(hase.gibY() >480) {
                    vy=0;
                    stehen();
                }
                
                if(hindernis1.gibX() <= hase.gibX() && hase.gibY() >= 460) {
                    if(hindernis1.gibR() == 100){
                        setzeHindernisAnzahl();                        
                        loescheHindernis1();
                        stehen();
                        sterben();
                    }
                    if(hindernis1.gibX() > 35) {
                        setzeHindernisAnzahl();                        
                        loescheHindernis1();
                        verliereLeben();
                        setzeLebenAnzahl();
                    }
                }
                
                if(hindernis2.gibX() <= hase.gibX() && hase.gibY() >= 460) { 
                    if(hindernis2.gibR() == 100){
                        setzeHindernisAnzahl();                        
                        loescheHindernis2();
                        stehen();
                        sterben();
                    }                 
                    if(hindernis2.gibX() > 35) {
                        setzeHindernisAnzahl();
                        loescheHindernis2();
                        verliereLeben();
                        setzeLebenAnzahl();
                    }
                }
                

                
                if(hindernis1.gibX() <= 0) {
                    setzeHindernisAnzahl();
                    loescheHindernis1();
                }
                
                if(hindernis2.gibX() <= 0) {
                    setzeHindernisAnzahl();
                    loescheHindernis2();

                }
                
                if(hindernis == 0){
                    setzeLevel();
                }                
            }
         });
         uhr.start();
    }
        
    public void starte() {uhr.start();}
    public void stoppe() {uhr.stop();}
    
    public void setzeHindernisAnzahl()
    {
        hindernis--;
        labelHindernisse.setText("Hindernisse: Noch "+hindernis);
    }
    
    public void verliereLeben(){
        hase = new HASE(50, 480, 10, "rot");
        refreshAll();
    }
    
    public void setzeLebenAnzahl()
    {
        leben--;
        labelLeben.setText("Leben: "+leben+"  |  ");
    }
    
    public void setzeLevel()
    {
        level++;
        hindernis = 10 * level;
        labelLevel.setText("Level: " + level + " | ");
        labelHindernisse.setText("Hindernisse: Noch "+hindernis);        
        vHindernis=-4-(level*2);
    }
    
    public void sterben()
    {
        if(leben > 0) leben = 0;
        fensterStatus = "NochmalSpielen";
        zeigeNochmalSpielen();
    }
       
    public void springen()
    {
        vy=-4;
    }
    
    public void stehen()
    {
        vy = 0;
        refresh();
        if(vSpucke == 0)
            spucke = new SPUCKE(65, 470, 2, "blau");
        hase = new HASE(50, 480, 10, "grau");
    }
    
    public void refreshAll(){
        refresh();
        hase.zeichne();
    }
    
    public void refresh(){
        hindernis1.loesche();
        hindernis2.loesche();
        hase.loesche();
        hintergrund = new HINTERGRUND();    
        hindernis1.zeichne();
        hindernis2.zeichne();
    }
     
      
    
    public void loescheHindernis1()
    {        
        ZEICHENFENSTER.gibFenster().loescheRechteck(hindernis1.gibX(),hindernis1.gibY(),hindernis1.gibR(),hindernis1.gibR());
        refreshAll();
        if(hindernis % 7 == 6)
            hindernis1 = new HINDERNIS(1800 - (level * 25 % 8),400,100,"rot");
        else
            hindernis1 = new HINDERNIS(1050,460,40,"grau");
    }
    
    public void loescheHindernis2()
    {                
        ZEICHENFENSTER.gibFenster().loescheRechteck(hindernis2.gibX(),hindernis2.gibY(),hindernis2.gibR(),hindernis2.gibR()); 
        refreshAll();
        if(hindernis % 3 == 2 )
            hindernis2 = new HINDERNIS(855 + (80 % level),400,100,"rot");
        else
            hindernis2 = new HINDERNIS(620,460,40,"schwarz");
    }
    
    
    public void bewegung()
    {
        refresh();
        hindernis1.bewegeX(vHindernis);
        hindernis2.bewegeX(vHindernis);
        spucke.bewegeX(vSpucke);
        if(vSpucke == 0)
            spucke.bewegeY(vy);
        hase.bewegeY(vy);

    }
     
    public void keyTyped( KeyEvent e ){
    }
    
    public void keyPressed( KeyEvent e ) 
{
    if (e.getKeyCode() == KeyEvent.VK_RIGHT)
    {
        vHindernis=-4-(level*2);
    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT)
    {
        vHindernis=0;          
    }
    
    if (e.getKeyCode() == KeyEvent.VK_UP)
    {
        if(vy == 0)  springen();
    }
    
    if (e.getKeyCode() == KeyEvent.VK_S)
    {
        vSpucke = 5;
    }
    
    if (e.getKeyCode() == KeyEvent.VK_ENTER)
    {
        uhr.stop();        
        if (fensterStatus == "Einleitung") {
            fensterStatus = "Anleitung";  
            zeigeAnleitung();
        }
            
        else if (fensterStatus == "Anleitung") {
            fensterStatus = "Spielen";  
            erstelleLabel();
            starteSpiel();
        }
        
        else if (fensterStatus == "NochmalSpielen") {
            fensterStatus = "Spielen";  
            resetLabel();
            starteSpiel();
        }
        
        
    }
}

public void keyReleased( KeyEvent e ) 
{
    if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
    {
        System.exit(0);
    }
    
    if (e.getKeyCode() == KeyEvent.VK_RIGHT)
    {
    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT)
    {
    }   
    

    
}
    
}
