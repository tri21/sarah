public class KREIS extends SPIELAUTOMAT
{
    
    public KREIS(int x, int y, int r, String f)
    {
        xPos=x;
        yPos=y;
        radius=r;
        farbe=f;
     
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos, yPos, radius, farbe);
        
    }

    public void loesche()
    {
        ZEICHENFENSTER.gibFenster().loescheKreis(xPos,yPos,radius);
    }
   
    public void zeichne()
    {
        ZEICHENFENSTER.gibFenster().fuelleKreis(xPos,yPos,radius,farbe);
    }
    
}
