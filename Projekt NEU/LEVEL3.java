import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class LEVEL3 implements KeyListener
{

    private SPIELAUTOMAT k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16,k17,k18,k19,k20,k21,k22,k23,k24,k25,k26,k27,k28,k29,k30,k31,k32,k33,k34,k35,k36;
    private Timer uhr, uhr2, uhr3;
    private int vx, vy, vx2, vy2, vx3, vx4, vy5, leben, kugeln, level, kasten, gegner;
    private String farbe;
    private JLabel label1, label2, label3, label4, label5;
    private boolean endlosEin=true;
   
    public LEVEL3(){
        k1 = new RECHTECK(500,460,40,"schwarz");
        k2 = new RECHTECK(1000,460,40,"schwarz");
       
        //Beine
        k3 = new RECHTECK(30,483,20, "schwarz");
        k4 = new RECHTECK(30,463,20, "schwarz");
        k5 = new RECHTECK(60,483,20, "schwarz");
        k6 = new RECHTECK(60,463,20, "schwarz");
        //Körper
        k7 = new RECHTECK(30,423,50, "schwarz");
        //Kopf
        k8 = new KREIS(55,403,25, "schwarz");
        //Arme
        k9 = new RECHTECK(80,433,15, "schwarz");
        k10 = new RECHTECK(15,433,15, "schwarz");
        //Hände
        k11 = new KREIS(13,440,10, "schwarz");
        k12 = new KREIS(95,440,10, "schwarz");
        //Pistole
        k13 = new RECHTECK(100, 440, 10, "rot");
        k14 = new RECHTECK(100, 430, 10, "rot");
        k15 = new RECHTECK(110, 430, 10, "rot");
        k16 = new RECHTECK(120, 430, 10, "rot");
        //KUGEL
        k17 = new KREIS(120, 435, 5, "rot");
       
       
        //Gegner
        //Beine
        k18 = new RECHTECK(1070,483,20, "schwarz");   
        k19 = new RECHTECK(1070,463,20, "schwarz");
        k20 = new RECHTECK(1100,483,20, "schwarz");  
        k21 = new RECHTECK(1100,463,20, "schwarz");    
        //Körper
        k22 = new RECHTECK(1070,423,50, "schwarz");   
        //Kopf
        k23 = new KREIS(1095,403,25, "schwarz");    
        //Arme
        k24 = new RECHTECK(1120,433,15, "schwarz");   
        k25 = new RECHTECK(1055,433,15, "schwarz");   
        //Hände
        k26 = new KREIS(1045,440,10, "schwarz");      
        k27 = new KREIS(1135,440,10, "schwarz");
        //Pistole
        k28 = new RECHTECK(1040, 440, 10, "rot");
        k29 = new RECHTECK(1040, 430, 10, "rot");
        k30 = new RECHTECK(1030, 430, 10, "rot");
        k31 = new RECHTECK(1020, 430, 10, "rot");
        //KUGEL
        k32 = new KREIS(1020, 435, 5, "rot");
        //Hinderniss
        k33 = new RECHTECK(1100,0,10,"schwarz");
        k34 = new KREIS(1105,5,5,"rot");
        //k35 = new RECHTECK(
        //k36 = new KREIS(
        
       
        leben = 5;
        level = 2;
        kugeln = 0;
        kasten = 10;
        gegner = 5;
       
        label1 = new JLabel("  Level "+level+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(label1, "unten");
        label2 = new JLabel("Leben: "+leben+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(label2, "unten");
        label3 = new JLabel("Verbrauchte Kugeln: "+kugeln+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(label3, "unten");
        label4 = new JLabel("Kästen: Noch "+kasten+"  |  ");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(label4, "unten");
        label5 = new JLabel("Gegner: Noch "+gegner+"");
        ZEICHENFENSTER.gibFenster().komponenteHinzufuegen(label5, "unten");
       
        vx=0;
        vy=0;
        vx2=0;
        vy2=0;
        vx3=0;
        vx4=0;
        vy5=0;
       
        ZEICHENFENSTER.gibFenster().gibFrame().addKeyListener(this);
       
        uhr=new Timer(50, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                bewegung();
            }
         });
         uhr.start();
        
         uhr2=new Timer(4000, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                schuss();
            }
         });
         uhr2.start();
         
         
         uhr3=new Timer(10000, new ActionListener()
         {
            public void actionPerformed(ActionEvent e)
            {
                sturz();
            }
         });
         uhr3.start();
    }
    
    public void starte() {uhr.start();}
    public void stoppe() {uhr.stop();}
    
        public void endlosschleife()
    {
        while(endlosEin) 
        {
         System.out.println(k17.gibX());
         System.out.println(k32.gibX());
         int kglX, kglY;
         kglX=k17.gibX();
         kglY=k17.gibY();
        
         if(kglY<=350) {vx=0; vy=3;}
         if(kglY<=450) {vx3=-5;}
         if(k3.gibY()>=483) {vy=0; vx=0; vx3=0;}
        
         if(k34.gibY()>=500) {k34.setAusgangspunktSturz(1105,5);}
         
         if(kglX==k22.gibX())
          {
             vx2=0;
             loescheKugel();
             loescheGegner();
             k17.setAusgangspunktKugel(k16.gibX(), k16.gibY()+5);
             k18.setAusgangspunktGegner(970,483);
             k19.setAusgangspunktGegner(970,463);
             k20.setAusgangspunktGegner(1000,483);
             k21.setAusgangspunktGegner(1000,463);
             k22.setAusgangspunktGegner(970,423);
             k23.setAusgangspunktGegner(995,403);
             k24.setAusgangspunktGegner(1020,433);
             k25.setAusgangspunktGegner(955,433);
             k26.setAusgangspunktGegner(945,440);
             k27.setAusgangspunktGegner(1035,440);
             k28.setAusgangspunktGegner(940,440);
             k29.setAusgangspunktGegner(940,430);
             k30.setAusgangspunktGegner(930,430);
             k31.setAusgangspunktGegner(920,430);
             k32.setAusgangspunktGegner(920,435);
          }
          else
          {if(kglX>=610)
            {
                vx2=0;
                loescheKugel();
                k17.setAusgangspunktKugel(k16.gibX(), k16.gibY()+5);
            }
          }
         
         if(k32.gibX()==k7.gibX())
           {
               vx4=0;
               loescheKugel2:
               loescheFigur();
               k32.setAusgangspunktKugel2(k31.gibX(),k31.gibY()+5);
            }
           
         if(k32.gibX()<=-10)
           {
               vx4=0;
               loescheKugel2:
               loescheFigur();
               k32.setAusgangspunktKugel2(k31.gibX(),k31.gibY()+5);
            }
         
         if(k1.gibX()<=-40) {loescheKasten1(); k1.setAusgangspunktKasten(1000,460);}
         if(k2.gibX()<=-40) {loescheKasten2(); k2.setAusgangspunktKasten(1000,460);}
        if(gegner==0) 
         {
             endlosEin=false;
             uhr.stop();
         }
        }
    }
   
        public void schießen()
    {
        vx2=10;
    }
    
    public void sturz()
    {
        vy5=30;
    }
   
    public void schuss()
    {
        if (k32.gibX()<=600) {vx4=-10;}
    }
   
        public void tot()
    {
       if(k17.gibX()>=k22.gibX()) {loescheGegner();}
    }
   
    public void springen()
    {
        vy=-4; vx=0;
    }
   
        public void ducken()
    {
        loescheFigur();
        knien();
    }
   
    public void stehen()
    {
        loescheFigur();
        //Beine
        k3 = new RECHTECK(30,483,20, "schwarz");
        k4 = new RECHTECK(30,463,20, "schwarz");
        k5 = new RECHTECK(60,483,20, "schwarz");
        k6 = new RECHTECK(60,463,20, "schwarz");
        //Körper
        k7 = new RECHTECK(30,423,50, "schwarz");
        //Kopf
        k8 = new KREIS(55,403,25, "schwarz");
        //Arme
        k9 = new RECHTECK(80,433,15, "schwarz");
        k10 = new RECHTECK(15,433,15, "schwarz");
        //Hände
        k11 = new KREIS(13,440,10, "schwarz");
        k12 = new KREIS(95,440,10, "schwarz");
        //Pistole
        k13 = new RECHTECK(100, 440, 10, "rot");
        k14 = new RECHTECK(100, 430, 10, "rot");
        k15 = new RECHTECK(110, 430, 10, "rot");
        k16 = new RECHTECK(120, 430, 10, "rot");
        //KUGEL
        k17 = new KREIS(120, 435, 5, "rot");
    }
   
        public void knien()
    {
        k3 = new RECHTECK(30,463,50,"schwarz");
        k4 = new RECHTECK(30,463,50,"schwarz");
        k5 = new RECHTECK(30,463,50,"schwarz");
        k6 = new RECHTECK(30,463,50,"schwarz");
        k7 = new RECHTECK(30,463,50,"schwarz");
        k8 = new RECHTECK(30,463,50,"schwarz");
        k9 = new RECHTECK(30,463,50,"schwarz");
        k10 = new RECHTECK(30,463,50,"schwarz");
        k11 = new RECHTECK(30,463,50,"schwarz");
        k12 = new RECHTECK(30,463,50,"schwarz");
        k13 = new RECHTECK(30,463,50,"schwarz");
        k14 = new RECHTECK(30,463,50,"schwarz");
        k15 = new RECHTECK(30,463,50,"schwarz");
        k16 = new RECHTECK(30,463,50,"schwarz");
        k17 = new RECHTECK(30,463,50,"schwarz");
    }
   
    public void loescheKugel()
    {
        ZEICHENFENSTER.gibFenster().loescheKreis(k17.gibX(),k17.gibY(),k17.gibR());
    }
   
    public void loescheKugel2()
    {
        ZEICHENFENSTER.gibFenster().loescheKreis(k32.gibX(),k32.gibY(),k32.gibR());
    }
   
    public void loescheGegner()
    {
        ZEICHENFENSTER.gibFenster().loescheRechteck(k18.gibX(),k18.gibY(),k18.gibR(),k18.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k19.gibX(),k19.gibY(),k19.gibR(),k19.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k20.gibX(),k20.gibY(),k20.gibR(),k20.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k21.gibX(),k21.gibY(),k21.gibR(),k21.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k22.gibX(),k22.gibY(),k22.gibR(),k22.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k23.gibX(),k23.gibY(),k23.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k24.gibX(),k24.gibY(),k24.gibR(),k24.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k25.gibX(),k25.gibY(),k25.gibR(),k25.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k26.gibX(),k26.gibY(),k26.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k27.gibX(),k27.gibY(),k27.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k28.gibX(),k28.gibY(),k28.gibR(),k28.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k29.gibX(),k29.gibY(),k29.gibR(),k29.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k30.gibX(),k30.gibY(),k30.gibR(),k30.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k31.gibX(),k31.gibY(),k31.gibR(),k31.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k32.gibX(),k32.gibY(),k32.gibR());
        gegner=gegner-1;
        label5.setText("Gegner: Noch "+gegner+"");
    }
   
    public void loescheFigur()
    {
        ZEICHENFENSTER.gibFenster().loescheRechteck(k3.gibX(),k3.gibY(),k3.gibR(),k3.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k4.gibX(),k4.gibY(),k4.gibR(),k4.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k5.gibX(),k5.gibY(),k5.gibR(),k5.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k6.gibX(),k6.gibY(),k6.gibR(),k6.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k7.gibX(),k7.gibY(),k7.gibR(),k7.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k8.gibX(),k8.gibY(),k8.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k9.gibX(),k9.gibY(),k9.gibR(),k9.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k10.gibX(),k10.gibY(),k10.gibR(),k10.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k11.gibX(),k11.gibY(),k11.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k12.gibX(),k12.gibY(),k12.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k13.gibX(),k13.gibY(),k13.gibR(),k13.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k14.gibX(),k14.gibY(),k14.gibR(),k14.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k15.gibX(),k15.gibY(),k15.gibR(),k15.gibR());
        ZEICHENFENSTER.gibFenster().loescheRechteck(k16.gibX(),k16.gibY(),k16.gibR(),k16.gibR());
        ZEICHENFENSTER.gibFenster().loescheKreis(k17.gibX(),k17.gibY(),k17.gibR());
       
        leben=leben-1;
        label2.setText("Leben: "+leben+"  |  ");
    }
       
    public void loescheKasten1()
    {
        ZEICHENFENSTER.gibFenster().loescheRechteck(k1.gibX(),k1.gibY(),k1.gibR(),k1.gibR());
    }
   
    public void loescheKasten2()
    {
        ZEICHENFENSTER.gibFenster().loescheRechteck(k2.gibX(),k2.gibY(),k2.gibR(),k2.gibR());
    }
   
        public void bewegung()
    {
        k1.bewegeX(vx3);
        k2.bewegeX(vx3);
        k3.bewegeY(vy);
        k4.bewegeY(vy);
        k5.bewegeY(vy);
        k6.bewegeY(vy);
        k7.bewegeY(vy);
        k8.bewegeY(vy);
        k9.bewegeY(vy);
        k10.bewegeY(vy);
        k11.bewegeY(vy);
        k12.bewegeY(vy);
        k13.bewegeY(vy);
        k14.bewegeY(vy);
        k15.bewegeY(vy);
        k16.bewegeY(vy);
        k17.bewegeY(vy);
        k17.bewegeY(vy2);
        k17.bewegeX(vx2);
        k18.bewegeX(vx3);
        k19.bewegeX(vx3);
        k20.bewegeX(vx3);
        k21.bewegeX(vx3);
        k22.bewegeX(vx3);
        k23.bewegeX(vx3);
        k24.bewegeX(vx3);
        k25.bewegeX(vx3);
        k26.bewegeX(vx3);
        k27.bewegeX(vx3);
        k28.bewegeX(vx3);
        k29.bewegeX(vx3);
        k30.bewegeX(vx3);
        k31.bewegeX(vx3);
        k32.bewegeX(vx3);
        k32.bewegeX(vx4);
        k33.bewegeX(vx2);
        k34.bewegeY(vy5);
        k34.bewegeX(vx3);
    }
       
    public void keyTyped( KeyEvent e ){
    }
   
    public void keyPressed( KeyEvent e )
{
    if (e.getKeyCode() == KeyEvent.VK_RIGHT)
    {
        k1.bewegeX(-5);
        k2.bewegeX(-5);
        k18.bewegeX(-5);
        k19.bewegeX(-5);
        k20.bewegeX(-5);
        k21.bewegeX(-5);
        k22.bewegeX(-5);
        k23.bewegeX(-5);
        k24.bewegeX(-5);
        k25.bewegeX(-5);
        k26.bewegeX(-5);
        k27.bewegeX(-5);
        k28.bewegeX(-5);
        k29.bewegeX(-5);
        k30.bewegeX(-5);
        k31.bewegeX(-5);
        k32.bewegeX(-5);
        k33.bewegeX(-5);
        k34.bewegeX(-5);
    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT)
    {
        k1.bewegeX(5);
        k2.bewegeX(5);
        k18.bewegeX(5);
        k19.bewegeX(5);
        k20.bewegeX(5);
        k21.bewegeX(5);
        k22.bewegeX(5);
        k23.bewegeX(5);
        k24.bewegeX(5);
        k25.bewegeX(5);
        k26.bewegeX(5);
        k27.bewegeX(5);
        k28.bewegeX(5);
        k29.bewegeX(5);
        k30.bewegeX(5);
        k31.bewegeX(5);
        k32.bewegeX(5);
        k33.bewegeX(5);
        k34.bewegeX(5);
    }
   
   
   
    if (e.getKeyCode() == KeyEvent.VK_UP)
    {
      if (k3.gibY()>=360)  { springen(); }
      else if (k3.gibY()<=60) { ducken(); }
    }
 
    if (e.getKeyCode() == KeyEvent.VK_UP)
    {
        springen();
    }
    if (e.getKeyCode() == KeyEvent.VK_DOWN)
    {
        ducken();
    }
   
    if (e.getKeyCode() == KeyEvent.VK_A)
    {
        schießen();
    }
}

public void keyReleased( KeyEvent e )
{
    if (e.getKeyCode() == KeyEvent.VK_RIGHT)
    {
        k1.bewegeX(-5);
        k2.bewegeX(-5);
        k18.bewegeX(-5);
        k19.bewegeX(-5);
        k20.bewegeX(-5);
        k21.bewegeX(-5);
        k22.bewegeX(-5);
        k23.bewegeX(-5);
        k24.bewegeX(-5);
        k25.bewegeX(-5);
        k26.bewegeX(-5);
        k27.bewegeX(-5);
        k28.bewegeX(-5);
        k29.bewegeX(-5);
        k30.bewegeX(-5);
        k31.bewegeX(-5);
        k32.bewegeX(-5);
        k33.bewegeX(-5);
        k34.bewegeX(-5);
    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT)
    {
        k1.bewegeX(5);
        k2.bewegeX(5);
        k18.bewegeX(5);
        k19.bewegeX(5);
        k20.bewegeX(5);
        k21.bewegeX(5);
        k22.bewegeX(5);
        k23.bewegeX(5);
        k24.bewegeX(5);
        k25.bewegeX(5);
        k26.bewegeX(5);
        k27.bewegeX(5);
        k28.bewegeX(5);
        k29.bewegeX(5);
        k30.bewegeX(5);
        k31.bewegeX(5);
        k32.bewegeX(5);
        k33.bewegeX(5);
        k34.bewegeX(5);
    }
   
    if (e.getKeyCode() == KeyEvent.VK_UP)
    {
        springen();
    }
    if (e.getKeyCode() == KeyEvent.VK_DOWN)
    {
        stehen();
    }
   
    if (e.getKeyCode() == KeyEvent.VK_A)
    {
        schießen();
        kugeln=kugeln+1;
        label3.setText("Verbrauchte Kugeln: "+kugeln+"  |  ");
    }
}
   
}