
public class RECHTECK extends SPIELAUTOMAT
{
    
    public RECHTECK(int x, int y, int r, String f)
    {
        xPos=x;
        yPos=y;
        radius=r;
        farbe=f;
     
        ZEICHENFENSTER.gibFenster().fuelleRechteck(xPos, yPos, radius, radius, farbe);
    }

    public void loesche()
    {
        ZEICHENFENSTER.gibFenster().loescheRechteck(xPos,yPos,radius,radius);
    }
    public void zeichne()
    {
        ZEICHENFENSTER.gibFenster().fuelleRechteck(xPos,yPos,radius,radius,farbe);
    }
}
